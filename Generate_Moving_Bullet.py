from numpy import random
import pygame
import cfg


class Moving_Bullet:
    def __init__(self, xplayer, yplayer, type):
        #bullet type
        self.type = type
        if type == "red":
            self.color = cfg.red_bullet_color
        elif type == "blue":
            self.color = cfg.blue_bullet_color
        # Size
        self.width = cfg.bullet_width
        self.height = cfg.bullet_height
        if type == "red":
            self.vel = cfg.red_bullet_velocity
        elif type == "blue":
            self.vel = cfg.blue_bullet_velocity


        # Position start possible
        self.ran = random.randint(1, 4, 1)
        if self.ran == 1:
            position = [-self.width, random.randint(-self.height, cfg.screen_heigh)]
        elif self.ran == 2:
            position = [random.randint(-self.width, cfg.screen_width), -self.height]
        elif self.ran == 3:
            position = [cfg.screen_width, random.randint(-self.height, cfg.screen_heigh)]
        else:
            position = [random.randint(-self.width, cfg.screen_width), cfg.screen_heigh]

        # Exact start position
        self.x = position[0]
        self.y = position[1]

        self.deltaX = (xplayer - self.x)
        self.deltaY = (yplayer - self.y)
        # Velocity in ortogonal directions
        self.xVel = float(format(((self.deltaX / (abs(self.deltaX) + abs(self.deltaY))) * self.vel), '.1f'))
        self.yVel = float(format(((self.deltaY / (abs(self.deltaY) + abs(self.deltaX))) * self.vel), '.1f'))



    def refresh(self):
        from main import screen
        return pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height))

    def increeseAmmount(self):
        pass
