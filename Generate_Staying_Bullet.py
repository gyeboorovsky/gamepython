from numpy import random
import pygame
import cfg

class Staying_Bullet:
    def __init__(self, type):
        self.type = type
        if type == "yellow":
            self.color = cfg.yellow_bullet_color
        elif type == "pink":
            self.color = cfg.pink_bullet_color
        self.width = cfg.bullet_width
        self.height = cfg.bullet_height
        self.x = random.randint(0, cfg.screen_width-self.width)
        self.y = random.randint(0, cfg.screen_heigh-self.height)


    def refresh(self):
        from main import screen
        return pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height))