import pygame
import os
#Context config
screen_possition_x = 1920
screen_possition_y = 0
screen_width = 1920
screen_heigh = 1080
screen_color = (10, 32, 46)
buttons_color = (180, 0, 0)
delay = 100
loose_live_pause_time = 500
screen = pygame.display.set_mode((screen_width, screen_heigh))
os_environ = os.environ['SDL_VIDEO_WINDOW_POS'] = '{}, {}'.format(1920, 0)

#Player config
player_start_x = 225
player_start_y = 225
player_width = 10
player_height = 10
player_vel = 3
player_color = (51, 204, 51)
eating_player_color = (255, 255, 255)
warning_player_color = ()


#Red bullet config
bullet_width = 10
bullet_height = 10
red_bullet_color = (255, 0, 0)
red_bullet_velocity = 1.6
red_bullet_generate_frequency = 256
immune_after_death_time = 100

#Blue bullet config
blue_bullet_color = (0, 255, 255)
blue_bullet_velocity = 1.5
eat_time = 600
blue_bullet_generate_frequency = 1000
warning_time = 200

#Yellow bullet config
yellow_bullet_color = (255, 255, 0)

#Pink bullet config
pink_bullet_color = (255, 0, 255)