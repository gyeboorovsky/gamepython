from db.db_connection import *

class Score:
    def __init__(self, player, score, level):
        self.nickname = player
        self.score = score
        self. level = level
        self.conn = open_database_connection()
        self.c = self.conn.cursor()

    def save_score(self):
        self.c.execute("INSERT INTO Results VALUES (NULL, ?, ?, ?, NULL)", (self.nickname, self.score, self.level))
        close_database_connection(self.conn)
