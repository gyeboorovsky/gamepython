import sqlite3


def open_database_connection():
    conn = sqlite3.connect('db/pythonsqlite.db')
    return conn

def close_database_connection(conn):
    conn.commit()
    conn.close()