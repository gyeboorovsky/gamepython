import screens_controller
from db.db_connection import open_database_connection, close_database_connection

class User:
    def __init__(self, user_name):
        self.nickname = user_name
        self.conn = open_database_connection()
        self.c = self.conn.cursor()


    def join(self):
        if not self.user_already_exist():
            self.create_new_user()
        self.pass_userId_to_screens_controller()
        close_database_connection(self.conn)

    def user_already_exist(self):
        self.c.execute("SELECT Nickname FROM Players")
        x = self.c.fetchall()
        nickname_list = []
        for i in range(len(x)):
            nickname_list.append(x[i][0])
        if self.nickname in nickname_list:
            return True
        return False

    def create_new_user(self):
        self.c.execute("INSERT INTO Players VALUES (NULL, ?)", (self.nickname,))

    def pass_userId_to_screens_controller(self):
        self.c.execute("SELECT Player_ID FROM Players WHERE Nickname=?", (self.nickname,))
        screens_controller.player_id = self.c.fetchone()[0]