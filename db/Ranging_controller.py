from db.db_connection import open_database_connection, close_database_connection

class Ranking:
    def __init__(self):
        self.conn = open_database_connection()
        self.c = self.conn.cursor()

    #TODO in get_ranking code can be shorter, and quary only one
    def get_ranking(self):
        self.c.execute(
            "SELECT Player, max(Score) as max_player_score from Results Group by Player order by max(Score) desc LIMIT 10")
        top_players_id = self.c.fetchall()
        # return top_players_id
        player = []
        top_players = []
        # list of player id
        id_list = []
        for i in top_players_id:
            id_list.append(int(i[0]))

        for i in top_players_id:
            self.c.execute("SELECT Nickname FROM Players WHERE Player_ID = ?", (str(i[0]),))
            player.append(self.c.fetchone()[0])
            player.append(i[1])
            top_players.append(player)
            player = []
        return top_players

        close_database_connection(self.conn)