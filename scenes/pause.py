import pygame
from pygame.locals import *
from cfg import screen_width, buttons_color, delay
from hudClass import Displayed_text
from functions import draw_text
def pause():
    from main import screen
    click = False
    run = True
    while run:

        hud = Displayed_text(screen, 70, (255, 255, 255))
        hud.display("Pause", screen_width / 2 - 150, 20)

        mx, my = pygame.mouse.get_pos()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    run = False
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        button_1 = pygame.Rect(50, 100, 200, 50)
        if button_1.collidepoint((mx, my)):
            if click:
                break
        pygame.draw.rect(screen, (buttons_color), button_1)
        draw_text('Continue', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 110)

        button_2 = pygame.Rect(50, 200, 200, 50)
        if button_2.collidepoint((mx, my)):
            if click:
                return 1
        pygame.draw.rect(screen, (buttons_color), button_2)
        draw_text('Menu', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 210)

        button_3 = pygame.Rect(50, 300, 200, 50)
        if button_3.collidepoint((mx, my)):
            if click:
                pygame.quit()
        pygame.draw.rect(screen, (buttons_color), button_3)
        draw_text('Quit', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 310)

        pygame.display.update()
        pygame.time.delay(delay)