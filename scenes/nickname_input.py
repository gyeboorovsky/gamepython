import pygame as pg
import screens_controller
from db.User_controller import User
from functions import draw_text
from db.Ranging_controller import Ranking

def nickname_input(screen):
    font = pg.font.Font(None, 200)
    clock = pg.time.Clock()
    input_box = pg.Rect(500, 400, 600, 130)
    color_inactive = pg.Color('lightskyblue3')
    color_active = pg.Color('dodgerblue2')
    color = color_inactive
    active = True
    text = 'INPUT NICK HERE'
    start_writing = False
    done = False
    ranking = Ranking().get_ranking()

    while not done:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                done = True
                color = color_active if active else color_inactive
            if event.type == pg.KEYDOWN:
                if active:
                    soundObj = pg.mixer.Sound('sounds/click.wav')
                    soundObj.play()
                    if not start_writing:
                        text = ''
                        start_writing = True
                    if event.key == pg.K_RETURN:
                        print(text)
                        screens_controller.nickname = text
                        User(text).join()
                        screens_controller.nickname_input = 0
                        screens_controller.main_menu = 1
                        done = True
                        break
                    if event.key == pg.K_ESCAPE:
                        pg.quit()
                    elif event.key == pg.K_BACKSPACE:
                        text = text[:-1]
                    elif event.key == pg.K_SPACE:
                        continue
                    else:
                        text += event.unicode
                        text = text.upper()
        screen.fill((10, 32, 46))

        draw_text("GRA TOMKA", font, pg.Color('dodgerblue2'), screen, 500, 50)
        draw_text("NICK:", font, pg.Color('lightskyblue3'), screen, 100, 400)

        draw_text("TOP PLAYERS:", pg.font.SysFont(None, 50), pg.Color('dodgerblue2'), screen, 530, 600)
        counter = 1
        text_height = 650
        for i in ranking:
            draw_text(str(counter) + ". " + i[0], pg.font.SysFont(None, 40), pg.Color('lightskyblue3'), screen, 500, text_height)
            draw_text(str(i[1]), pg.font.SysFont(None, 50), pg.Color('lightskyblue3'), screen, 900, text_height)
            draw_text("pkt", pg.font.SysFont(None, 50), pg.Color('lightskyblue3'), screen, 1000, text_height)
            text_height += 40
            counter += 1
        # Render the current text.
        txt_surface = font.render(text, True, color)
        # Resize the box if the text is too long.
        width = max(200, txt_surface.get_width()+30)
        input_box.w = width
        # Blit the text.
        screen.blit(txt_surface, (input_box.x+5, input_box.y+5))
        # Blit the input_box rect.
        pg.draw.rect(screen, color, input_box, 2)

        pg.display.flip()
        clock.tick(30)
