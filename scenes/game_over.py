import pygame
from pygame.locals import *
from cfg import screen_color, screen_width, delay
from hudClass import Displayed_text
from functions import draw_text
import screens_controller
from db.Ranging_controller import Ranking


def game_over(points):
    from main import screen
    display = False
    game_over = True
    scene_title = Displayed_text(screen, 70, (255, 255, 255))
    content_title = Displayed_text(screen, 50, (255, 255, 255))
    content = Displayed_text(screen, 50, pygame.Color('lightskyblue3'))

    while True:

        #if prevents refreshing items on screen every frame
        if not display:
            screen.fill((screen_color))
            scene_title.display("Game Over", screen_width / 2 - 150, 20)
            # display earned point
            content.display(screens_controller.nickname + ", you've got: " + str(points) + " points", 300, 300)

            content_title.display("TOP PLAYERS:", 330, 400)
            ranking = Ranking().get_ranking()
            print(ranking)
            counter = 1
            text_height = 450
            for i in ranking:
                content.display(str(counter) + ". " + i[0], 300, text_height)
                content.display(str(i[1]), 700, text_height)
                content.display("pkt", 850, text_height)
                text_height += 50
                counter += 1
            display = True

        mx, my = pygame.mouse.get_pos()

        click = False
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                if event.key == K_RETURN:
                    screens_controller.game = 1
                    screens_controller.game_over = 0
                    game_over = False
                    break
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
        if not game_over:
            break

        button_1 = pygame.Rect(50, 100, 200, 50)
        if button_1.collidepoint((mx, my)):
            if click:
                screens_controller.game = 1
                screens_controller.game_over = 0
                break
        pygame.draw.rect(screen, (255, 0, 0), button_1)
        draw_text('Start Game', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 100)

        button_2 = pygame.Rect(50, 200, 200, 50)
        if button_2.collidepoint((mx, my)):
            if click:
                pygame.quit()
        pygame.draw.rect(screen, (255, 0, 0), button_2)
        draw_text('Quit', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 200)

        button_3 = pygame.Rect(50, 300, 200, 50)
        if button_3.collidepoint((mx, my)):
            if click:
                screens_controller.nickname_input = 1
                screens_controller.game_over = 0
                break
        pygame.draw.rect(screen, (255, 0, 0), button_3)
        draw_text('New Player', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 300)

        for event in pygame.event.get():
            click = False
            if event.type == QUIT:
                pygame.quit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        pygame.time.delay(delay)