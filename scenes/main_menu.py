import pygame
from pygame.locals import *
from cfg import screen_color, screen_width, buttons_color, delay, red_bullet_color, player_color, blue_bullet_color, pink_bullet_color, yellow_bullet_color
from functions import draw_text
import screens_controller
from db.Ranging_controller import Ranking

def main_menu():
    from main import screen
    click = False
    menu = True
    display = False
    while menu:
        #text on screen
        if not display:
            screen.fill((screen_color))
            draw_text('Main Menu', pygame.font.SysFont(None, 70), (255, 255, 255), screen, screen_width / 2 - 150, 20)

            draw_text("TOP PLAYERS:", pygame.font.SysFont(None, 50), (255, 255, 255),
                      screen, 330, 400)
            ranking = Ranking().get_ranking()
            print(ranking)
            counter = 1
            text_height = 450
            for i in ranking:
                draw_text(str(counter) + ". " + i[0], pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 300,
                          text_height)
                draw_text(str(i[1]), pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 700, text_height)
                draw_text("pkt", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 800, text_height)
                text_height += 50
                counter += 1
            display = True

            draw_text("HOW TO PLAY:", pygame.font.SysFont(None, 50), (255, 255, 255), screen, 1000, 400)

            draw_text("you are GREEN", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 1000, 450)
            pygame.draw.rect(screen, player_color, pygame.Rect(970, 465, 10, 10))

            draw_text("avoid RED", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 1000, 500)
            pygame.draw.rect(screen, red_bullet_color, pygame.Rect(970, 515, 10, 10))

            draw_text("eat YELLOW to spawn BLUE or PINK", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 1000, 550)
            pygame.draw.rect(screen, yellow_bullet_color, pygame.Rect(970, 565, 10, 10))

            draw_text("eat BLUE to eat evetyrhing for 5 secound", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 1000, 600)
            pygame.draw.rect(screen, blue_bullet_color, pygame.Rect(970, 615, 10, 10))

            draw_text("eat PINK to fall a RED bullets ", pygame.font.SysFont(None, 50), pygame.Color('lightskyblue3'), screen, 1000, 650)
            pygame.draw.rect(screen, pink_bullet_color, pygame.Rect(970, 665, 10, 10))

        mx, my = pygame.mouse.get_pos()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
            if event.type == KEYDOWN:
                soundObj = pygame.mixer.Sound('sounds/click.wav')
                soundObj.play()
                if event.key == K_ESCAPE:
                    pygame.quit()
                if event.key == K_RETURN:
                    screens_controller.game = 1
                    screens_controller.main_menu = 0
                    menu = False
                    break
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
        button_1 = pygame.Rect(50, 100, 200, 50)
        if button_1.collidepoint((mx, my)):
            if click:
                screens_controller.game = 1
                screens_controller.main_menu = 0
                break
        pygame.draw.rect(screen, (buttons_color), button_1)
        draw_text('Start Game', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 110)

        button_2 = pygame.Rect(50, 200, 200, 50)
        if button_2.collidepoint((mx, my)):
            if click:
                pygame.quit()
        pygame.draw.rect(screen, (buttons_color), button_2)
        draw_text('Quit', pygame.font.SysFont(None, 50), (255, 255, 255), screen, 50, 210)

        for event in pygame.event.get():
            click = False
            if event.type == QUIT:
                pygame.quit()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True

        pygame.display.update()
        pygame.time.delay(delay)