import pygame
from pygame.locals import *

import Generate_Staying_Bullet
from cfg import screen, screen_width, screen_heigh, player_width, player_height, screen_color, delay
import cfg
from scenes.pause import pause
from db.Score_controller import Score
import screens_controller
from functions import increse_level
from hudClass import Displayed_text
from playerClass import Player

def game(player_x, player_y):
    import Generate_Moving_Bullet
    from datetime import datetime
    bullets = []
    frame_count = 0
    bullets_level_count = 0
    pink_timer = 0
    points = 0
    run = 3
    game_mode = False
    game_mode_was_on = False
    code = ''
    notice = []
    notice_timer = 0
    player = Player()
    red_bullet_generate_frequency = 100
    player_x = player.position_x
    player_y = player.position_y
    go_to_menu = 0

    while run > 0:

        bullets_to_delete_index = []
        screen.fill(screen_color)
        # time is needed to generate equal frame rate
        frame_start = int(datetime.now().strftime("%Y%m%d%H%M%S%f")[:-2])
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    soundObj = pygame.mixer.Sound('sounds/click.wav')
                    soundObj.play()
                    go_to_menu = pause()
                    break
                if event.key == K_BACKSPACE:
                    code = ''
                else:
                    code += event.unicode

        #codes
        if code == 'k':
            run -= 1
            code = ''
        #codemode
        if game_mode == False:
            if code == 'gm':
                game_mode = True
                code = ''
                pygame.mixer.music.stop()
                game_mode_was_on = True
        else:
            if code == 'gm':
                game_mode = False
                code = ''
            if code == 'b':
                bullets.append(Generate_Moving_Bullet.Moving_Bullet(player_x, player_y, "blue"))
                code = ''
            if code == 'y':
                bullets.append(Generate_Staying_Bullet.Staying_Bullet("yellow"))
                code = ''
            if code == 'r':
                bullets.append(Generate_Moving_Bullet.Moving_Bullet(player_x, player_y, "red"))
                code = ''
            if code == 'p':
                bullets.append(Generate_Staying_Bullet.Staying_Bullet("pink"))
                code = ''
            if code == 'l':
                run += 10
                code = ''

        player_x, player_y = player.refresh(frame_count)

        ##HUD
        hud = Displayed_text(screen, 32, (255, 255, 255))
        # score
        hud.display("Score: " + str(points), 50, 50)
        # level
        hud.display("Level: " + str(bullets_level_count), 1720, 50)
        # lives
        hud.display("Lives: " + str(run), 1720, 950)
        # notice
        if notice_timer > frame_count:
            hud.display((notice[-1]), 50, 950)

        print(red_bullet_generate_frequency)
        if frame_count % 1000 == 0:
            bullets_level_count += 1

        #spawn red wave
        red_bullet_generate_frequency = increse_level(frame_count)
        if frame_count % red_bullet_generate_frequency == 0:
            bullets.append(Generate_Moving_Bullet.Moving_Bullet(player_x, player_y, "red"))
        #spawn yellow
        if frame_count % 1000 == 0:
            bullets.append(Generate_Staying_Bullet.Staying_Bullet("yellow"))

        collided = -1
        for i in range(bullets.__len__()):
            bullets[i].refresh()

            if pink_timer == frame_count and bullets[i].type == "red":
                bullets[i].color = (255, 100, 100)
                bullets[i].xVel = -bullets[i].xVel
                bullets[i].yVel = -bullets[i].yVel


            if bullets[i].type != "yellow" and bullets[i].type != "pink":
                bullets[i].x += bullets[i].xVel
                bullets[i].y += bullets[i].yVel

                # del bullet out of screen
                if (bullets[i].y + player_height <= 0 or
                        bullets[i].y >= screen_heigh or
                        bullets[i].x + player_width <= 0 or
                        bullets[i].x >= screen_width):
                    points += 1
                    bullets_to_delete_index.append(i)

            collision = player.collision_check(bullets[i])
            if collision:
                bullets_to_delete_index.append(i)
            ##take yellow
            if collision == "yellow":
                pts, note, spawn = player.collision_yellow()
                points += pts
                notice.append(note)
                if spawn < 2:
                    bullets.append(Generate_Moving_Bullet.Moving_Bullet(player_x, player_y, "blue"))
                else:
                    bullets.append(Generate_Staying_Bullet.Staying_Bullet("pink"))
            ##take blue
            elif collision == "blue":
                pts, note = player.collision_blue(frame_count)
                points += pts
                notice.append(note)
            ##take pink
            elif collision == "pink":
                note, pink_taken = player.collision_pink()
                notice.append(note)
                if pink_taken:
                    pink_timer = frame_count + 1
            ##hit red
            elif collision == "red":
                run, pts = player.collision_red(frame_count)
                if pts:
                    points += pts

        frame_count += 1

        pygame.display.update()

        #deleting bullets
        if bullets_to_delete_index:
            for i in bullets_to_delete_index:
                try:
                    del bullets[i]
                except:
                    print("Can't del bullet", i)
        #clock to rebuild
        while True:
            frameStop = int(datetime.now().strftime("%Y%m%d%H%M%S%f")[:-2])
            if frameStop - frame_start >= delay:
                break

        if go_to_menu:
            screens_controller.game = 0
            screens_controller.main_menu = 1
            break

    while game_mode_was_on:
        print("Gamemode was ON")

    cfg.red_bullet_velocity = 1
    #saving score to database
    player = screens_controller.player_id
    score = points
    level = bullets_level_count
    Score(player, score, level).save_score()


    if not go_to_menu:
        screens_controller.game = 0
        screens_controller.game_over = 1
        screens_controller.points = points
