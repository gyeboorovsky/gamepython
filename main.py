import os
import pygame
import screens_controller


from cfg import screen, player_start_y
from scenes.game import game
from scenes.game_over import game_over
from scenes.main_menu import main_menu
from scenes.nickname_input import nickname_input



pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.init()

os.environ['SDL_VIDEO_WINDOW_POS'] = '{}, {}'.format(1920, 0)

pygame.display.set_caption("First Game")

#music
pygame.mixer.music.load('sounds/music_background.wav')
pygame.mixer.music.play(-1)

while True:
    if screens_controller.nickname_input:
        nickname_input(screen)
    elif screens_controller.main_menu:
        main_menu()
    elif screens_controller.game:
        game(player_start_y, player_start_y)
    elif screens_controller.game_over:
        game_over(screens_controller.points)