import pygame


def display_text(displayedText, color, x, y, size):
    from main import screen
    font = pygame.font.Font('freesansbold.ttf', size)
    text = font.render(displayedText, True, color)
    screen.blit(text, (x, y))


def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, 1, color)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)

def increse_level(frame):
    frame /= 3
    if frame > -1 and frame < 200:
        return 100
    elif frame >= 200 and frame < 300:
        return 60
    elif frame >= 300 and frame < 400:
        return 40
    elif frame >= 400 and frame < 500:
        return 30
    elif frame >= 500 and frame < 600:
        return 20
    elif frame >= 600 and frame < 700:
        return 15
    elif frame >= 700 and frame < 800:
        return 12
    elif frame >= 800 and frame < 900:
        return 10
    elif frame >= 900 and frame < 2000:
        return 8
    elif frame >= 2000 and frame < 3000:
        return 6
    elif frame >= 3000 and frame < 5000:
        return 5
    elif frame >= 5000 and frame < 7000:
        return 4
    elif frame >= 7000 and frame < 10000:
        return 3
    elif frame >= 10000 and frame < 30000:
        return 2
    else:
        return 1