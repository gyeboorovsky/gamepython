import pygame
import random
from cfg import player_height, \
    player_width, \
    player_color, \
    player_start_x, \
    player_start_y, \
    player_vel, \
    screen_width, \
    screen_heigh, \
    screen, \
    bullet_width, \
    bullet_height, \
    eat_time, \
    immune_after_death_time, \
    warning_time

class Player:
    def __init__(self):
        self.screen = screen
        self.screen_width = screen_width
        self.screen_heihgt = screen_heigh
        self.width = player_width
        self.height = player_height
        self.position_x = player_start_x
        self.position_y = player_start_y
        self.color = player_color
        self.state = 'normal'
        self.lives = 3
        self.velocity = player_vel
        self.state_timer = 0
        self.rest_of_state_time = 0

    def moving(self):
        keys = pygame.key.get_pressed()
        if (keys[pygame.K_LEFT] or keys[pygame.K_a]) and self.position_x > 0:
            self.position_x -= self.velocity
        if (keys[pygame.K_RIGHT] or keys[pygame.K_d]) and self.position_x < (self.screen_width - self.width):
            self.position_x += self.velocity
        if (keys[pygame.K_UP] or keys[pygame.K_w]) and self.position_y > 0:
            self.position_y -= self.velocity
        if (keys[pygame.K_DOWN] or keys[pygame.K_s]) and self.position_y < (self.screen_heihgt - self.height):
            self.position_y += self.velocity


    def refresh(self, frame):
        self.moving()

        if self.state_timer < frame:
            self.state = 'normal'
            self.color = player_color
        elif self.state == 'blue' and self.state_timer < frame + warning_time:
            if int(frame / 10) % 2 == 0:
                self.color = (255, 0, 0)#player_color
            else:
                self.color = (255, 255, 255)
        if self.state == 'normal': #normal
            pygame.draw.rect(self.screen,self.color,(self.position_x, self.position_y, self.width, self.height))
        elif self.state == 'blue': #eating
            pygame.draw.rect(self.screen,self.color,(self.position_x, self.position_y, self.width, self.height))
        return self.position_x, self.position_y

    def change_state(self, type, frame):
        if type == 'blue':
            self.color = (255, 255, 255)
            self.state = 'blue'

    def collision_check(self, bullet):
        if not (bullet.y + bullet_height < self.position_y or
                bullet.y > self.position_y + player_height or
                bullet.x + bullet_width < self.position_x or
                bullet.x > self.position_x + player_width):
            return bullet.type


    def collision_yellow(self):
        soundObj = pygame.mixer.Sound('sounds/get_yellow.wav')
        soundObj.play()
        points = 50
        notice = 'Look for BLUE or PINK'
        random_spawn = random.randint(0, 3)
        return points, notice, random_spawn

    def collision_blue(self, frame):
        soundObj = pygame.mixer.Sound('sounds/get_blue.wav')
        soundObj.play()
        points = 100
        notice = 'EAT EVERYTHING!!!'
        if self.state_timer > frame:
            self.rest_of_state_time = self.state_timer - frame
        else:
            self.change_state('blue', frame)
        self.state_timer = frame + eat_time + self.rest_of_state_time
        return points, notice

    def collision_pink(self):
        soundObj = pygame.mixer.Sound('sounds/get_pink.wav')
        soundObj.play()
        notice = 'RED bullets go down'
        return notice, True

    def collision_red(self, frame):
        if self.state == 'blue':
            soundObj = pygame.mixer.Sound('sounds/get_red.wav')
            soundObj.play()
            points = 50
            return self.lives, points
        elif frame > self.state_timer:
            soundObj = pygame.mixer.Sound('sounds/death.wav')
            soundObj.play()
            self.state_timer = frame + immune_after_death_time
            self.lives -= 1
            pygame.time.wait(300)
            return self.lives, None
        else:
            return self.lives, None