import pygame

class Hud:
    def __init__(self, screen, font_size, font_color):
        self.screen = screen
        self.font_size = font_size
        self.font_color = font_color

class Displayed_text(Hud):
    def display(self, content, x, y):
        font = pygame.font.Font('freesansbold.ttf', self.font_size)
        text = font.render(content, True, self.font_color)
        self.screen.blit(text, (x, y))